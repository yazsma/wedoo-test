import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs/observable/of';
import { ShopService } from '../../core/services/shop.service';
import { HomeComponent } from './home.component';

const EXACT_AMOUNT_ANSWER = {
  equal: {
    value: 42,
    cards: [22, 20],
  },
  floor: {
    value: 42,
    cards: [22, 20],
  },
  ceil: {
    value: 42,
    cards: [22, 20],
  },
};

const IN_BETWEEN_AMOUNT_ANSWER = {
  floor: {
    value: 22,
    cards: [22],
  },
  ceil: {
    value: 25,
    cards: [25],
  },
};

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let shopService: ShopService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientTestingModule],
      declarations: [HomeComponent],
      providers: [ShopService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    shopService = TestBed.get(ShopService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return possible cards values when reaching amount is possible', () => {
    // Given
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(EXACT_AMOUNT_ANSWER)
    );
    component.amount = 42;

    // When
    component.getCardsMatching();

    // Then
    expect(component.matchingCardsValues).toEqual(
      EXACT_AMOUNT_ANSWER.equal.cards
    );
  });

  it('should display possible cards values when reaching amount is possible', async () => {
    // Given
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(EXACT_AMOUNT_ANSWER)
    );
    component.amount = 42;

    // When
    component.getCardsMatching();

    // Then
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const cardsDiv = fixture.debugElement.query(By.css('#cards'));
      expect(cardsDiv.nativeElement.textContent).toContain(
        EXACT_AMOUNT_ANSWER.equal.cards[0]
      );
      expect(cardsDiv.nativeElement.textContent).toContain(
        EXACT_AMOUNT_ANSWER.equal.cards[1]
      );
    });
  });

  it('should propose possible values when amount is in between possible values', () => {
    // Given
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(IN_BETWEEN_AMOUNT_ANSWER)
    );
    component.amount = 23;

    // When
    component.getCardsMatching();

    // Then
    expect(component.possibleAmountsInfo).toEqual({
      floor: IN_BETWEEN_AMOUNT_ANSWER.floor,
      ceil: IN_BETWEEN_AMOUNT_ANSWER.ceil,
    });
  });

  it('should display possible ceil and floor when amount is in between what is possible', () => {
    // Given
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(IN_BETWEEN_AMOUNT_ANSWER)
    );
    component.amount = 23;

    // When
    component.getCardsMatching();

    // Then
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const floorButton = fixture.debugElement.query(By.css('#floor'));
      expect(floorButton.nativeElement.value).toContain(
        IN_BETWEEN_AMOUNT_ANSWER.floor.value
      );
      const ceilButton = fixture.debugElement.query(By.css('#ceil'));
      expect(ceilButton.nativeElement.value).toContain(
        IN_BETWEEN_AMOUNT_ANSWER.ceil.value
      );
    });
  });

  it('should set the typed amount to the chosen amount when user makes a choice for floor value', () => {
    // Given
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(IN_BETWEEN_AMOUNT_ANSWER)
    );
    component.amount = 23;

    // When
    component.getCardsMatching();
    component.chooseAmount(component.possibleAmountsInfo.floor);

    // Then
    expect(component.amount).toEqual(IN_BETWEEN_AMOUNT_ANSWER.floor.value);
  });

  it('should set the typed amount to the chosen amount when user makes a choice for ceil value', () => {
    // Given
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(IN_BETWEEN_AMOUNT_ANSWER)
    );
    component.amount = 23;

    // When
    component.getCardsMatching();
    component.chooseAmount(component.possibleAmountsInfo.ceil);

    // Then
    expect(component.amount).toEqual(IN_BETWEEN_AMOUNT_ANSWER.ceil.value);
  });

  it('should display matching card values when user makes a choice for ceil value', () => {
    // Given
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(IN_BETWEEN_AMOUNT_ANSWER)
    );
    component.amount = 23;

    // When
    component.getCardsMatching();
    component.chooseAmount(component.possibleAmountsInfo.ceil);

    // Then
    expect(component.matchingCardsValues).toEqual(
      IN_BETWEEN_AMOUNT_ANSWER.ceil.cards
    );
  });

  it('should auto correct the user amount when it is lower than possible value', () => {
    // Given
    const higherAmount = {
      ceil: {
        value: 20,
        cards: [20],
      },
    };
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(higherAmount)
    );
    component.amount = 5;

    // When
    component.getCardsMatching();

    // Then
    expect(component.amount).toEqual(20);
  });

  it('should auto correct the user amount when it is higher than possible value', () => {
    // Given
    const lowerAmount = {
      floor: {
        value: 70,
        cards: [35, 35],
      },
    };
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(lowerAmount)
    );
    component.amount = 90;

    // When
    component.getCardsMatching();

    // Then
    expect(component.amount).toEqual(70);
  });

  it('should automatically set matching cards values when selected amount is higher than possible value', () => {
    // Given
    const lowerAmount = {
      floor: {
        value: 70,
        cards: [35, 35],
      },
    };
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(lowerAmount)
    );
    component.amount = 90;

    // When
    component.getCardsMatching();

    // Then
    expect(component.matchingCardsValues).toEqual([35, 35]);
  });

  it('should emit amount as output when reaching amount is possible', () => {
    // Given
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(EXACT_AMOUNT_ANSWER)
    );
    spyOn(component.amountSelected, 'emit').and.stub();
    component.amount = 42;

    // When
    component.getCardsMatching();

    // Then
    expect(component.amountSelected.emit).toHaveBeenCalledWith(42);
  });

  it('should emit amount as output when amount is in beteween possible amounts', () => {
    // Given
    spyOn(shopService, 'getCardsAmountsMatching').and.returnValue(
      of(IN_BETWEEN_AMOUNT_ANSWER)
    );
    spyOn(component.amountSelected, 'emit').and.stub();
    component.amount = 37;

    // When
    component.getCardsMatching();
    component.chooseAmount(component.possibleAmountsInfo.ceil);

    // Then
    expect(component.amountSelected.emit).toHaveBeenCalledWith(25);
  });
});
