import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ShopService } from '../../core/services/shop.service';
import { AmountInformation } from '../../shared/models/amount-information.model';
import { AmountBounds } from '../../shared/models/amount-bound.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  @Output() amountSelected: EventEmitter<number> = new EventEmitter();

  isAmountInBetween = false; // States if selected amount is in between possible values
  amount = 0;
  matchingCardsValues: number[] = [];
  possibleAmountsInfo: AmountBounds;

  constructor(private shopService: ShopService) {}

  ngOnInit() {}

  /**
   * Calls backend shop service to retrieve cards matching the selected amount
   * Handles possible cases (perfect matching or amount higher/in between/lower than possible cards values)
   */
  public getCardsMatching(): void {
    this.shopService
      .getCardsAmountsMatching(this.amount, 5)
      .subscribe((matchingAmounts) => {
        this.handleMatchingAmounts(matchingAmounts);
      });
  }

  /**
   * Sets displayed amount to amount selected by user
   * Sets displayed cards to the cards matching the selected amount
   * Emits amount value
   * @param amountInfo amount selected by the user
   */
  public chooseAmount(amountInfo: AmountInformation): void {
    this.setMatchingAmountInfo(amountInfo);
    this.isAmountInBetween = false;
  }

  /**
   * Handles possible cases (perfect matching or amount higher/in between/lower than possible cards values)
   * @param amountBounds possible amounts bounds (equal, ceil, floor)
   */
  private handleMatchingAmounts(amountBounds: AmountBounds) {
    this.isAmountInBetween =
      amountBounds.ceil && amountBounds.floor && !amountBounds.equal;

    if (this.isAmountInBetween) {
      // Selected amount is in between possible cards values
      this.possibleAmountsInfo = {
        floor: amountBounds.floor,
        ceil: amountBounds.ceil,
      };
    } else {
      // Selected amount is equal to a possible value or out of bounds
      const matchingBound = amountBounds.equal
        ? amountBounds.equal
        : amountBounds.ceil
        ? amountBounds.ceil
        : amountBounds.floor;
      this.setMatchingAmountInfo(matchingBound);
    }
  }

  /**
   * Sets amount and cards with the given amount information
   * Emits the amount as an output
   * @param amountInfo amount information
   */
  private setMatchingAmountInfo(amountInfo: AmountInformation) {
    this.amount = amountInfo.value;
    this.matchingCardsValues = amountInfo.cards;
    this.amountSelected.emit(amountInfo.value);
  }
}
