import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { FormsModule } from '@angular/forms';
import { ShopService } from '../../core/services/shop.service';

@NgModule({
  imports: [SharedModule, HomeRoutingModule],
  providers: [ShopService],
  declarations: [HomeComponent],
})
export class HomeModule {}
