import { AmountInformation } from './amount-information.model';
import { AmountBounds } from './amount-bound.model';

describe('AmountBounds', () => {
  it('should create', () => {
    const amountInfo = new AmountInformation(30, [15, 15]);
    expect(new AmountBounds(amountInfo)).toBeTruthy();
  });
});
