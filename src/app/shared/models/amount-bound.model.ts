import { AmountInformation } from './amount-information.model';

export class AmountBounds {
  constructor(
    public equal?: AmountInformation,
    public floor?: AmountInformation,
    public ceil?: AmountInformation
  ) {}
}
