import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AmountBounds } from '../../shared/models/amount-bound.model';

@Injectable()
export class ShopService {
  constructor(private httpClient: HttpClient) {}

  /**
   * Calls backend shop service to retrieve cards amounts information which match given amount
   * @param {number} amount amount to match
   * @param {number} shopId id of the shop to get the cards from
   */
  getCardsAmountsMatching(
    amount: number,
    shopId: number
  ): Observable<AmountBounds> {
    const httpOptions = {
      params: new HttpParams().set('amount', amount.toString()),
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: 'tokenTest123',
      }),
    };

    return this.httpClient.get<AmountBounds>(
      `${environment.SHOP_SERVICE_URL}/${shopId}/search-combination`,
      httpOptions
    );
  }
}
