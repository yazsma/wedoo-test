import { TestBed, inject } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ShopService } from './shop.service';
import { environment } from '../../../environments/environment';

describe('ShopService', () => {
  let shopService: ShopService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShopService],
      imports: [HttpClientTestingModule],
    });

    httpTestingController = TestBed.get(HttpTestingController);
    shopService = TestBed.get(ShopService);
  });

  it('should be created', () => {
    expect(shopService).toBeTruthy();
  });

  it('should call backend shop service with GET when getCardsMatching is called', () => {
    const expectedAmountsInformation = {
      ceil: {
        value: 20,
        cards: [20],
      },
    };
    environment.SHOP_SERVICE_URL = 'http://localhost:3000/shop';

    shopService.getCardsAmountsMatching(15, 5).subscribe((amountsInfo) => {
      expect(amountsInfo).toEqual(expectedAmountsInformation);
    });

    const req = httpTestingController.expectOne(
      'http://localhost:3000/shop/5/search-combination?amount=15'
    );

    expect(req.request.method).toEqual('GET');

    req.flush(expectedAmountsInformation);
  });
});
